import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/components/Home'
import Signup from '@/components/Signup'
import Profile from '@/components/Profile'
import Course from '@/components/Course'
import HomeLoggedIn from '@/components/HomeLoggedIn'
import Signin from '@/components/Signin'
import ContentBook from '@/components/ContentBook'
import Worksheet from '@/components/Worksheet'
import LessonPlan from '@/components/LessonPlan'
import AudioVideo from '@/components/AudioVideo'
import Notification from '@/components/Notification'
import About from '@/components/About'
import Chat from '@/components/Chat'
import DemoClass from '@/components/DemoClass'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home
    },
    {
      path: '/signup',
      name: 'SignUp',
      component: Signup
    },
    {
      path: '/profile',
      name: 'Profile',
      component: Profile
    },
    {
      path: '/course',
      name: 'Course',
      component: Course
    },
    {
      path: '/homeLoggedIn',
      name: 'HomeLoggedIn',
      component: HomeLoggedIn
    },
    {
      path: '/signin',
      name: 'Signin',
      component: Signin
    },
    {
      path: '/contentbook',
      name: 'ContentBook',
      component: ContentBook
    },
    {
      path: '/worksheet',
      name: 'Worksheet',
      component: Worksheet
    },
    {
      path: '/lessonplan',
      name: 'LessonPlan',
      component: LessonPlan
    },
    {
      path: '/avsupport',
      name: 'AudioVideo',
      component: AudioVideo
    },
    {
      path: '/notification',
      name: 'Notification',
      component: Notification
    },
    {
      path: '/about',
      name: 'About',
      component: About
    },
    {
      path: '/chat',
      name: 'Chat',
      component: Chat
    },
    {
      path: '/demo',
      name: 'DemoClass',
      component: DemoClass
    }

  ]
})
